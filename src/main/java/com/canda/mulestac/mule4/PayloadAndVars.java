package com.canda.mulestac.mule4;

/**
 * mule4 message is immutable and so modifications of payload and variables needs
 * to be collected and set via
 * 	set-variable and set-payload
 * later.
 *
 * See <flow name="invokeJava"
 */
public class PayloadAndVars {
	// empty to have less dependencies
}
