package com.canda.mulestac.changeover;

import java.util.Map;

import org.mule.runtime.api.message.Message;
import org.mule.runtime.api.security.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.canda.mulestac.mule4.PayloadAndVars;

/**
 *
 */
public class Mule3AndMule4JavaWrapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(Mule3AndMule4JavaWrapper.class);

	public static PayloadAndVars invoke(Message aMessage, Map<String, Object> aVars, Authentication anAuthentication, String aCorrelationId, Error aError)
			throws Exception {
		LOGGER.info("aMessage=" + aMessage);
		LOGGER.info("aVars=" + aVars);
		LOGGER.info("anAuthentication=" + anAuthentication);
		LOGGER.info("aCorrelationId=" + aCorrelationId);
		LOGGER.info("aError=" + aError);
		// return invokePrivate(aMessage, aVars, anAuthentication, aCorrelationId, aError, new Mule3AndMule4JavaWorkerCreator());
		return null;
	}

}
